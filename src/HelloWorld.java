import java.sql.SQLOutput;

public class HelloWorld
{
    public static void main  (String[] args) {
        String firstName = "Nick";
        String surname = "Moore";

        String fullname = getFullName(firstName, surname);
        welcome(fullname);

        ifStatementCode();

        System.out.println("End of Program");
    }

    public static String getFullName(String firstName, String surname)
    {
        return firstName + " " + surname;
    }

    public static void printLine()
    {
        System.out.println("----------------------------");
    }

    public static void welcome(String name)
    {
        String output = "Hello Mr " + name + "!";
        printLine();
        System.out.println(output);
        printLine();
    }

    public static void ifStatementCode()
    {
        int x = 107;

        if (x < 10)
        {
            System.out.println("x is less than 10");
        }
        else if (x < 100)
        {
            System.out.println("x is between 10 and 100");
        }
        else
        {
            System.out.println("x is greater than 100");
        }

        String[] animals = {"cat", "cat", "dog", "tortoise", "cat", "rabbit", "dog", "cat", "dog", "cat"};

        int numDogs = 0;
        int numCats = 0;

        for (int i = 0; i < animals.length; i++)
        {
            if(animals[i] == "cat")
            {
                numCats++;
            }
            else if (animals[i] == "dog")
            {
                numDogs++;
            }
        }

        System.out.println("Number of cats: " + numCats);
        System.out.println("Number of dogs: " +numDogs);

        int[] ages = {24, 31, 29, 40, 18, 20, 42, 50};

        int max = ages[0];

        for (int i = 1; i < ages.length; i ++)
        {
            if (ages[i] > max)
            {
                max = ages[i];
            }
        }

        System.out.println("The maximum value is 50");

        int min = ages[0];

        for (int i = 1; i > ages.length; i ++)
        {
            if (ages[i] < min)
            {
                min = ages[i];
            }
        }

        System.out.println("The minimum value is 18");
    }
}

